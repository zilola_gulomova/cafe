import Vue from 'vue'
import VueRouter from 'vue-router'
import Cafe from '@/components/Cafe'
import CafeView from '@/components/CafeView'
import Contact from '@/views/Contact'
import About from '@/views/About'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Cafe',
    component: Cafe
  },
  {
    path: '/cafe/view',
    name: 'CafeView',
    component: CafeView
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
